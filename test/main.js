'use strict';

const Scratch = require('../src');

let scratch = new Scratch('./extension.json');

let b = true;
setInterval(() => {
  scratch.getReporter('brightness').value++;
  scratch.getReporter('slider').value = !b;
  scratch.getReporter('reporterWithParam').withParams("4").value++;
  scratch.getReporter('reporterWithParam').withParams("10").value++;
  scratch.getReporter('reporterWithParam2').withParams("10/test").value++;
  b = !b;
},1000);

scratch.getReporter('brightness').onReset = () => {
  scratch.getReporter('brightness').value = 1000;
}

scratch.getCommand('setVolume').onExec = p => {
  console.log(p);
  scratch.getReporter('brightness').value *= p[0];
};

scratch.getCommand('setVolume2').onExec = p => {
  return new Promise( (resolve, reject) => {
    console.log(p);
    resolve();
  });
};

scratch.getCommand('setVolume3').onExec = p => {
  return new Promise( (resolve, reject) => {
    console.log(p);
    scratch.getReporter('brightness').value /= p[0];
    setTimeout(() => resolve(true), 1000);
  });
};

scratch.start();
