'use strict';

module.exports = class ScratchBlockParameterSlot {
  static getId(params) {
    return params.join('/');
  }

  constructor(params, type) {
    this.id = ScratchBlockParameterSlot.getId(params);
    this.type = type;
    this.reset();
  }

  reset() {
    this._value = this.type === 'b' ? false : 0;
  }

  get value() {
    return this._value;
  }

  set value(v) {
    this._value = v;
  }
}
