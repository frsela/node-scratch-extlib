"use strict";

const parameterSlotRegex = /%[nsb]/;

module.exports = class ScratchBlock {
  // blockSpec: [ type, description, name, default params ]
  constructor(blockSpec) {
    this._spec = blockSpec;
    this.onReset = null;

    this.numParams = this.format.split(parameterSlotRegex).length - 1;
  }

  get type() {
    return this._spec[0];
  }

  get format() {
    return this._spec[1];
  }

  get name() {
    return this._spec[2];
  }

  get default_param_values() {
    return this._spec.slice(3);
  }

  get isReporter() {
    return this.type === 'r' || this.type === 'b';
  }

  get isCommand() {
    return !this.isReporter;
  }

  _addCallback(name, cb) {
    this[`cb_${name}`] = typeof cb === 'function' ? cb : () => {};
  }

  set onReset(cb) {
    this._addCallback('onReset', cb);
  }

  reset() {
    this.cb_onReset();
  }
};
