'use strict';

const ScratchBlockReporter = require('./scratch_block_reporter');
const ScratchBlockCommand = require('./scratch_block_command');

function scratch_block_factory(blockSpec) {
  switch (blockSpec[0]) {
    case ' ':   // Command block
    case 'w':   // Command block that waits
      return new ScratchBlockCommand(blockSpec);
    case 'r':   // Number reporter block
    case 'b':   // Boolean reporter block
      return new ScratchBlockReporter(blockSpec);

    default:
      return null;
  }
}

module.exports = scratch_block_factory;