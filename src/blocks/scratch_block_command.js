"use strict";

const ScratchBlock = require('./scratch_block');

module.exports = class ScratchBlockCommand extends ScratchBlock {
  constructor(blockSpec) {
    super(blockSpec);

    this.onExec = () => Promise.resolve();
    this.reset();
  }

  get isWait() {
    return this.type === 'w';
  }

  set onExec(cb) {
    this._addCallback('onExec', cb);
  }

  reset() {
    this.waiting_id = null;

    this.cb_onReset();
  }

  exec(waitId, params) {
    if (this.isWait) {
      this.waiting_id = waitId;
    }

    let isPromise = this.cb_onExec(params)
    if (Promise.resolve(isPromise) === isPromise) {
      isPromise
      .then(() => {
        if (this.isWait) {
          this.waiting_id = null;
        }
      });
    } else {
      this.waiting_id = null;
    }
  }

  get waiting() {
    return this.waiting_id;
  }
};
