"use strict";

const ScratchBlock = require('./scratch_block');
const ScratchBlockParameterSlot = require('./scratch_block_parameter_slot')

module.exports = class ScratchBlockReporter extends ScratchBlock {
  constructor(blockSpec) {
    super(blockSpec);

    this.parametersSlots = {};

    this.onReset = () => {
      this.getParams()
      .forEach(parametersSlot => parametersSlot.reset());
    };

    this.reset();
  }

  getParams() {
    return Object.keys(this.parametersSlots);
  }

  withParams(params) {
    if (!Array.isArray(params) && typeof params === 'string') {
      params = params.trim();
      params = params.length === 0 ? [] : params.split('/');
    }
    if (params.length !== this.numParams) {
      return null;
    }

    let parametersSlot = this.parametersSlots[ScratchBlockParameterSlot.getId(params)];
    if (!parametersSlot) {
      parametersSlot = this.parametersSlots[ScratchBlockParameterSlot.getId(params)] =
        new ScratchBlockParameterSlot(params, this.type);
    }
    return parametersSlot;
  }

  get value() {
    return this.numParams === 0 ? this.withParams([]).value : null;
  }

  set value(v) {
    if (this.numParams === 0) {
      this.withParams([]).value = v;
    }
  }
};
