'use strict';

const app = require('express')();
const server = require('http').Server(app);

function app_init() {
  app.use( (req, res, next) => {
    res.removeHeader("X-Powered-By");
    next();
  });
}

function app_error_manager() {
  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.path = req.originalUrl;
    err.status = 404;
    next(err);
  });

  // error handler
  // no stacktraces leaked to user unless in development environment
  app.use((err, req, res, next) => {
    const maybeNum = parseInt(err, 10);
    const error = (isNaN(maybeNum) ? (err.status || err.code) : maybeNum) || 500;
    switch (error) {
      case 400:
      case 401:
        res.status(error);
        err.msg && res.send(err.msg);
        res.end();
        break;

      case 403:
      case 404:
        res.status(error);
        res.send(err.msg);
        break;

      default:
        if (typeof error === 'string') {
          // Possible SQL error
          res.status(500);
        } else {
          res.status(error);
        }
        res.send(err.message || err.msg);
        res.end(err.stack && err.stack.split('\n'));
    }
  });
}

module.exports = class ScratchAPI {
  constructor(scratch) {
    this.scratch = scratch;

    app_init();

    // Scratch middleware
    app.use( (req, res, next) => {
      req.scratch = this.scratch;
      next();
    });

    app.use('/', require('./routers')(this.scratch));

    app_error_manager();
  }

  start() {
    return new Promise( (resolve, reject) => {
      server.on('error', e => {
        console.log(`SCRATCH Extension "${this.scratch.name}" REST API Error: `, e)
        reject(e);
      })
      server.listen(this.scratch.port, () => {
        console.log(`SCRATCH Extension "${this.scratch.name}" REST API listening on *:${this.scratch.port}`);
        resolve();
      });
    });
  }
};