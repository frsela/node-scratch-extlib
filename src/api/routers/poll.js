'use strict';

const router = require('express').Router();

router.use('/', (req, res, next) => {
  let response = '';

  function addResponse(name, value, params = "") {
    if (params.length > 0) {
      params = `/${params}`;
    }
    response += `${name}${params} ${value}\n`;
  }

  if (req.scratch.problem) {
    addResponse('_problem', req.scratch.problem);
  }

  Object.keys(req.scratch.reporter_blocks)
  .forEach(k => req.scratch.reporter_blocks[k].getParams()
    .forEach(p =>
      addResponse(k, req.scratch.reporter_blocks[k].withParams(p).value, p)
    )
  );

  // Waiting commands
  // Result not implemented since not yet implemented in Scratch
  let busy = '';
  Object.keys(req.scratch.command_blocks)
  .forEach(k => {
    if(req.scratch.command_blocks[k].isWait &&
       req.scratch.command_blocks[k].waiting) {
        busy += `${req.scratch.command_blocks[k].waiting} `;
    }
  });
  if (busy.length > 0) {
    addResponse('_busy', busy);
  }

  res.send(response);
});

module.exports = router;