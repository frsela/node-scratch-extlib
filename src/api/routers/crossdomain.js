'use strict';

const router = require('express').Router();

router.use('/', (req, res, next) => {
  res.send(`<cross-domain-policy>
  <allow-access-from domain="*" to-ports="${req.scratch.port}"/>
</cross-domain-policy>`);
});

module.exports = router;