'use strict';

const router = require('express').Router();

module.exports = scratch => {
  Object.keys(scratch.command_blocks)
  .forEach(c => {
    const uri = scratch.command_blocks[c].isWait ? `/${c}/:id` : `/${c}`;
    router.use(uri, (req, res, next) => {
      scratch.command_blocks[c].exec(
        req.params.id || null,
        req.url.split('/').slice(1)
      );
      res.end();
    });
  });

  return router;
};