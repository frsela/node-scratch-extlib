'use strict';

const router = require('express').Router();

router.use('/', (req, res, next) => {
  Object.keys(req.scratch.command_blocks)
  .forEach(k => req.scratch.command_blocks[k].reset());
  Object.keys(req.scratch.reporter_blocks)
  .forEach(k => req.scratch.reporter_blocks[k].reset());

  res.end();
});

module.exports = router;