'use strict';

const router = require('express').Router();

module.exports = scratch => {
  router.use('/poll', require('./poll'));
  router.use('/reset_all', require('./reset_all'));
  router.use('/crossdomain.xml', require('./crossdomain'));
  router.use('/', require('./commands')(scratch));

  return router;
};
