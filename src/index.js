'use strict';

const ScratchBlocks = require('./blocks');
const ScratchAPI = require('./api');

module.exports = class Scratch {
  constructor (scratch_extension_file) {
    this.extension = require(process.cwd() + '/' + scratch_extension_file);

    this.blocks = {
      commands: {},
      reporters: {}
    };
    this.extension.blockSpecs.forEach(blockSpec => {
      let block = ScratchBlocks(blockSpec);
      if (!block) {
        return;
      }
      this.blocks[block.isReporter ? 'reporters' : 'commands'][block.name] = block
    });
    // Ignoring "menus" section

    this.API = new ScratchAPI(this);

    console.log(this.extension)
  }

  start() {
    this.API.start();
  }

  get name() {
    return this.extension.extensionName;
  }

  get port() {
    return this.extension.extensionPort;
  }

  get command_blocks() {
    return this.blocks.commands;
  }

  get reporter_blocks() {
    return this.blocks.reporters;
  }

  getReporter(name) {
    return this.blocks.reporters[name];
  }

  getCommand(name) {
    return this.blocks.commands[name];
  }

  // Report problems to Scratch
  set problem(msg) {
    this._problem = msg;
  }

  get problem() {
    return this._problem;
  }
}